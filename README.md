# Acesse a nosssa plataforma hospedada no Heroku [Clicando Aqui!](https://storm-project.herokuapp.com/home)

# TUTORIAL PARA CONFIGURAÇÃO DA PLATAFORMA:

- Para instalar o Node.js use este comando ```sudo apt-get install nodejs```

- Para instalar o Npm (Node) use este comando ```sudo apt-get install npm```

- Clone o repositorio do projeto na sua maquina com este comando no terminal ```git clone git@gitlab.com:BDAg/storm-project.git```

- Na pasta appWeb/back-end dentro da pasta do projeto execute este comando no terminal ```sudo npm install```.

- Na pasta appWeb/front-end dentro da pasta do projeto execute este comando no terminal ```sudo npm install```.

- Agora iniciamos a API que esta na pasta appWeb/back-end com o comando ```sudo node server.js```.

- Agora iniciamos a plataforma que esta na pasta appWeb/front-end com o comando ```sudo ng serve```

- Acesse com seu navegador o site ```http://localhost:4200``` e pronto!

# Developers Team

#### <strong>GOSTARIA DE CONHECER QUEM SÃO OS MESTRES JEDI'S POR TRAS DOS CÓDIGOS??<br>[CLIQUE AQUI](https://gitlab.com/BDAg/storm-project/wikis/Team) E VENHA PARA O LADO NEGRO DA FORÇA!!</strong>

# Andamento do Projeto


# Andamento do Projeto

`ACESSE TODA NOSSA ESTRUTURA CLICANDO NO TÓPICO DESEJADO!!`

- [x] <b>[Definições de Projeto](https://gitlab.com/BDAg/storm-project/wikis/1.1-Defini%C3%A7%C3%B5es-de-Projeto)
- [x] <b>[Documentação da Solução](https://gitlab.com/BDAg/storm-project/wikis/1.2-documenta%C3%A7%C3%A3o-da-solu%C3%A7%C3%A3o)
- [x] <b>[Desenvolvimento da Solução (Sprint 1)](https://gitlab.com/BDAg/storm-project/wikis/1.3-desenvolvimento-da-solu%C3%A7%C3%A3o#sprint-1)
- [x] <b>[Desenvolvimento da Solução (Sprint 2)](https://gitlab.com/BDAg/storm-project/wikis/1.3-desenvolvimento-da-solu%C3%A7%C3%A3o#sprint-2)
- [x] <b>[Desenvolvimento da Solução (Sprint 3)](https://gitlab.com/BDAg/storm-project/wikis/1.3-desenvolvimento-da-solu%C3%A7%C3%A3o#sprint-3)
- [x] <b>[Desenvolvimento da Solução (Sprint 4)](https://gitlab.com/BDAg/storm-project/wikis/1.3-desenvolvimento-da-solu%C3%A7%C3%A3o#sprint-4)
- [x] <b>[Fechamento do Projeto](https://gitlab.com/BDAg/storm-project/wikis/1.4-fechamento-do-projeto)