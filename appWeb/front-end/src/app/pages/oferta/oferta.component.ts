import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OfertasComponent } from '../ofertas/ofertas.component'
import { Router } from '@angular/router';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.component.html',
  styleUrls: ['./oferta.component.css']
})
export class OfertaComponent implements OnInit {

  urlCall: string;
  public href: string = "";
  x: any;
  param: any;
  item: any;
  idOferta: any;

  constructor(private http: HttpClient, private Ofertas: OfertasComponent, private rotas: Router) { }

  listaOferta() {
    this.urlCall = `https://api-storm-project.herokuapp.com/ofertasFacebook/${this.x[2]}`
    return this.http.get(this.urlCall).subscribe(
      data => {
      this.item = data;
        this.item = Array.of(this.item);
        console.log(this.item)
      },
      err => console.error(err)
    );
  }

  getUrl() {
    this.href = this.rotas.url;
  }

  findGetParameter() {
    this.x = this.href.split("/");
  }

  ngOnInit() {
    this.getUrl()
    this.findGetParameter()
    //this.getId();
    this.listaOferta();
  }
}
