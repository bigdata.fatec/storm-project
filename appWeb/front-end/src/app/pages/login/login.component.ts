import { ApiService } from './../../services/api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuard } from '../../guards/auth.guard';
import { AuthService } from '../../auth.service';
import { first } from 'rxjs/operators';
import { element } from 'protractor';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message: string;
  email: string;
  senha: string;
  status: any;
  emailRecuperacao: string;
  returnUrl = '/perfil';

  nomeCompleto: string;
  emailCadastro: string;
  senhaCadastro: string;
  formCadastro: object;

  constructor(private router: Router, public authService: AuthService, private api: ApiService) { }

  ngOnInit() {
    this.returnUrl = '/ofertas';
  }

  login() {
    this.ocultarMensagem('divErroLogin');
    this.exibirMensagem('carregandoLogin');
    this.api.logar({ 'email': this.email, 'senha': this.senha }).subscribe(
      result => {
        this.status = result;
        //console.log(this.status);
        if (this.status.status === 'ok') {
          localStorage.setItem('access_token', this.status.token);
          localStorage.setItem('nome_completo', this.status.nomeCompleto);
          this.router.navigate(['/ofertas']);
          window.location.reload();
        } else {
          console.log('erro ao fazer login');
          this.ocultarMensagem('carregandoLogin');
          this.exibirMensagem('divErroLogin');
          setTimeout(() => {
            this.ocultarMensagem('divErroLogin');
          },
            5000);
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  esqueceuSenha() {
    this.ocultarMensagem('divErroEsqueceu')
    this.ocultarMensagem('divAcertoEsqueceu')
    this.exibirMensagem('carregandoEsqueceu')
    //console.log(this.emailRecuperacao)
    this.api.esqueceuSenha(this.emailRecuperacao).subscribe(
      result => {
        this.ocultarMensagem('carregandoEsqueceu')
        this.exibirMensagem('divAcertoEsqueceu');
        console.log(result);
      },
      err => {
        console.log(err);
        this.ocultarMensagem('carregandoEsqueceu')
        this.exibirMensagem('divErroEsqueceu');
        setTimeout(() => {
          this.ocultarMensagem('divErroEsqueceu');
        },
          5000);
      }
    )
  }

  cadastrar() {
    this.formCadastro = {
      nomeCompleto: this.nomeCompleto,
      email: this.emailCadastro,
      senha: this.senhaCadastro
    };
    this.ocultarMensagem('divErroCadastrar')
    this.exibirMensagem('carregandoCadastrar')
    this.api.cadastrar(this.formCadastro).subscribe((res) => {
      //console.log(res);
      if ('mensagem' in res) {
        console.log(res['mensagem']);
        this.ocultarMensagem('carregandoCadastrar')
        this.exibirMensagem('divErroCadastrar');
        setTimeout(() => {
          this.ocultarMensagem('divErroCadastrar');
        },
          5000);
      } else {
        //console.log('O token e');
        //console.log(res['token']);
        localStorage.setItem('isLoggedIn', 'true');
        localStorage.setItem('nome_completo', this.formCadastro['nomeCompleto']);
        localStorage.setItem('access_token', res['token']);
        this.router.navigate(['/perfil']);
        window.location.reload()
      }
    },
      err => {
        console.log(err);
      }
    );
  }

  exibirMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = false;
  }

  ocultarMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = true;
  }

}
