import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../services/api.service';
import { saveAs } from '../../../../node_modules/file-saver';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl } from '@angular/forms';
import * as CanvasJS from '../../../assets/canvasjs/canvasjs.min';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  profileForm = new FormGroup({
    emailUsuario: new FormControl(''),
    senhaUsuario: new FormControl(''),
  });

  token: any;
  senhaAntiga: any;
  senha: any;
  nomeCompleto: string;
  listaGrupos: any;

  constructor(private api: ApiService, private http: HttpClient) { }

  ngOnInit() {
    this.token = localStorage.getItem('access_token');
    this.nomeCompleto = localStorage.getItem('nome_completo');
    this.getValues();
  }

  exportarDados() {
    let url = "https://api-storm-project.herokuapp.com/ofertasFacebook";
    this.http.get(url, { responseType: 'blob' })
      .subscribe((res) => {
        saveAs(res, "dados.json")
      })
  }

  deletarUsuarios() {
    console.log(this.profileForm.get('emailUsuario').value)
    this.api.deletarUsuario(this.profileForm.get('emailUsuario').value, this.profileForm.get('senhaUsuario').value).subscribe(
      result => {
        console.log(result)
        localStorage.setItem('isLoggedIn', 'false');
        localStorage.removeItem('nome_completo');
        localStorage.removeItem('access_token');
        window.location.reload();
      },
      err => {
        console.log(err)
      }
    )
  }

  redefinirSenha() {
    this.ocultarMensagem('erro')
    this.ocultarMensagem('sucesso')
    this.exibirMensagem('carregando');
    this.api.redefinirSenha({ 'token': this.token, 'senhaAntiga': this.senhaAntiga, 'senhaNova': this.senha }).subscribe(
      result => {
        console.log(result)
        this.ocultarMensagem('carregando');
        this.exibirMensagem('sucesso');
        setTimeout(() => {
          this.ocultarMensagem('sucesso');
        },
          5000);
      },
      err => {
        console.log(err)
        this.ocultarMensagem('carregando');
        this.exibirMensagem('erro');
        setTimeout(() => {
          this.ocultarMensagem('erro');
        },
          5000);
      }
    );
  }

  getValues() {
    this.http.get('https://api-storm-project.herokuapp.com/gruposFacebook').subscribe(result => {
      this.listaGrupos = result;
      console.log(this.listaGrupos)
      console.log(this.listaGrupos[0]['nomeGrupos'])
      this.graficoPie();
    })
  }

  graficoPie() {
      let chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: false,
      title: {
        text: "Quantidade de Ofertas"
      },
      data: [{
        type: "pie",
        showInLegend: true,
        toolTipContent: "<b>{name}</b>: {y}",
        indexLabel: "{name} - {y}",
        dataPoints: [
          { y: this.listaGrupos[0]['quantidadeOfertas'], name: this.listaGrupos[0]['nomeGrupos'] ,},
          { y: this.listaGrupos[1]['quantidadeOfertas'], name: this.listaGrupos[1]['nomeGrupos'],},
          { y: this.listaGrupos[2]['quantidadeOfertas'], name: this.listaGrupos[2]['nomeGrupos'],},
        ]
      }]
    });
    chart.render();
  }

  exibirMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = false;
  }

  ocultarMensagem(idTag) {
    const mensagem = document.getElementById(idTag);
    mensagem.hidden = true;
  }

  switchHtml(ids) {
    const oldDiv = document.getElementsByClassName('opc');
    for (var i = 0; i < oldDiv.length; i++) {
      oldDiv[i].className = 'btn btn-secondary';
    }

    const div = document.getElementById(ids);
    div.className = 'btn btn-secondary active opc';
    if (ids == 'option1') {
      this.ocultarMensagem('graficos');
      this.ocultarMensagem('exportar');
      this.ocultarMensagem('deletar');
      this.exibirMensagem('exibirTrocarSenha');
    } else if (ids == 'option2') {
      this.ocultarMensagem('exportar');
      this.ocultarMensagem('deletar');
      this.ocultarMensagem('exibirTrocarSenha');
      this.exibirMensagem('graficos');
    } else if (ids == 'option3') {
      this.ocultarMensagem('graficos');
      this.ocultarMensagem('deletar');
      this.ocultarMensagem('exibirTrocarSenha');
      this.exibirMensagem('exportar');
    } else if (ids == 'option4') {
      this.ocultarMensagem('graficos');
      this.ocultarMensagem('exportar');
      this.ocultarMensagem('exibirTrocarSenha');
      this.exibirMensagem('deletar');
    }
  }
}