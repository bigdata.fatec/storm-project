const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const ofertasFacebookController = require("../controllers/gruposControllers");

router.get("/", ofertasFacebookController.listar)

module.exports = router;