const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const ofertasFacebookController = require("../controllers/ofertasFacebookController");

router.get("/", ofertasFacebookController.listar);
router.post("/", ofertasFacebookController.enviar);
router.get("/grupo/:IdGrupo",ofertasFacebookController.listarByGrupo)

router.get("/:Id", ofertasFacebookController.listarOne);
router.put("/:Id", ofertasFacebookController.editarOne);
router.delete("/:Id", ofertasFacebookController.deletarOne);

// module.exports = function(app) {
//     var ofertasFacebookController = require('../controllers/ofertasFacebookController');

//     app.route('/comentariosfacebook')
//         .get(ofertasFacebookController.listar)
//         .post(ofertasFacebookController.enviar);
    
//     app.route('/comentariosfacebook/:Id')
//         .get(ofertasFacebookController.listarOne)
//         .put(ofertasFacebookController.editarOne)
//         .delete(ofertasFacebookController.deletarOne);

// }

module.exports = router;