var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ofertasSchema = new Schema
                                (
                                    {
                                        nomePerfil : { type : String, required : true },
                                        linkPerfil : { type : String,required: true },
                                        nomeGrupo : { type : String, required : true },
                                        idGrupo : { type : String, required: true},
                                        linkGrupo : { type : String, required: true },
                                        textoOferta : { type : String, required : true },
                                        linkOferta : { type : String, required: true },
                                        fotosOferta : {type: Array, required: false},
                                        linkFotosHosp : {type: Array, required: false}
                                    }
                                );

module.exports = mongoose.model('ofertasFacebook', ofertasSchema, "ofertasFacebook");