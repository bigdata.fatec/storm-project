var mongoose = require('mongoose');
ofertasFacebook = require('../models/ofertasFacebook');

exports.listar = function(req, res) {
    ofertasFacebook.find({}, function(err, comentarios) {
        if (err)
            res.send(err);
        res.json(comentarios);
    });
};

exports.listarByGrupo = function(req, res) {
    ofertasFacebook.find({ idGrupo : req.params.IdGrupo }, function(err, comentario) {
        if (err)
            res.send(err);
        res.json(comentario);
    });
};

exports.enviar = function(req, res) {
    var novoComentario = new ofertasFacebook(req.body);
    novoComentario.save(function(err, comentario) {
        if (err)
            res.send(err);
        res.json(comentario);
    });
};

exports.listarOne = function(req, res) {
    ofertasFacebook.findById({ _id : req.params.Id }, req.body, { new : true }, function(err, comentario) {
        if (err)
            res.send(err);
        res.json(comentario);
    });
};

exports.editarOne = function(req, res) {
    ofertasFacebook.findOneAndUpdate({ _id : req.params.Id }, req.body, { new : true }, function(err, comentario) {
        if (err)
            res.send(err);
        res.json(comentario);
    });
};

exports.deletarOne = function(req, res) {
    ofertasFacebook.remove({ _id : req.params.Id }, function(err, comentario) {
        if (err)
            res.send(err);
        res.json({ 'message' : 'Apagado com sucesso!' });
    });
};