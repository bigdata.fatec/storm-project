from bs4 import BeautifulSoup
import requests 
import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from http.cookiejar import LWPCookieJar

conexao = pymongo.MongoClient('mongodb+srv://botFacebook:bc123456!@storm-project-eu6jj.mongodb.net/test')
mydb = conexao['StormProject']

global s
global contador

vetorImg = []

s = requests.Session()

def criaSession(login, senha): #Função para armazenamento de Cookies Locais
		s.cookies = LWPCookieJar('cookiejar')
		if not os.path.exists('cookiejar'):
			print('.:: Armazenando Cookies ::.')
			s.cookies.save()
			loginFacebook(login, senha)
		else:
			print('.:: Carregando Cookies ::.')
			s.cookies.load(ignore_discard=True)
			req = s.get('https://m.facebook.com/')
			soup = BeautifulSoup(req.content,'html.parser')
			
			textoSoup = str(soup)
			if textoSoup.find("Participe do Facebook") != 0:
				loginFacebook(login, senha)
		s.cookies.save(ignore_discard=True)

def loginFacebook(email,senha): #Função para Login no Facebook
	vetor1 = []
	vetor2 = []

	req = requests.get('https://m.facebook.com/')
	soup = BeautifulSoup(req.content,'html.parser')

	for x in soup.find_all('input'):
		requisicao = x['name']
		if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
			vetor1.append(requisicao)
			resposta = x['value']
			vetor2.append(resposta)
			
	dicionario = dict(zip(vetor1,vetor2))

	dicionario['email'] = email
	dicionario['pass'] = senha
	dicionario['login'] = 'Log In'

	r = s.post('https://m.facebook.com/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&amp;refid=8', data=dicionario)
	timeRandom = random.randint(8, 20)
	time.sleep(timeRandom)
	contador = 1
	buscaMongo = mydb.ofertasFacebook.find({'fotosColetadas' : False}) #Buscamos as Urls de cada foto no MongoDB
	for resultados in buscaMongo:
		linkPerfil = resultados['linkPerfil']
		#print ('.:: Encontrando Ofertas ::.')
		linksFotos = resultados['fotosOferta']
		for links in linksFotos:
			#print ('.:: Redirecionando Para Nova Foto ::.')
			firstReq = s.get(links)
			getFotosOfertas(firstReq,linkPerfil)
			time.sleep(timeRandom)
		if len(vetorImg) != 0:
			fotoThumb = vetorImg[0]
			enviarMongo(vetorImg,linkPerfil,fotoThumb)
			vetorImg.clear()
		else:
			fotoThumb = 'https://www.freeiconspng.com/uploads/no-image-icon-4.png'
			enviarMongo0(linkPerfil,fotoThumb)
			vetorImg.clear()
	print ('.:: Saindo Do Script ::.')

def getFotosOfertas(requisicao,linkPerfil):
	soupPrimario =  BeautifulSoup(requisicao.content,'html.parser')
	getLink = soupPrimario.select('div#MPhotoContent')
	for texto in getLink:
		soupPrimario = BeautifulSoup(str(texto),'html.parser')
		if str(soupPrimario.title) != 'Entrar no Facebook | Facebook':
			for x in soupPrimario.find_all('a'):
				if str(x.text) == 'Ver no tamanho original': #Procuramos o link para full resolution
					linkFull = 'https://m.facebook.com'+str(x['href'])
					url = s.get(linkFull)
					soupInterno = BeautifulSoup(url.content,'html.parser')
					findImages = soupInterno.find_all('a')
					#print ('.:: Buscando Fotos Do Post ::.')
					for xx in findImages:
						link = xx['href']
						#print (link)
						vetorImg.append(link)
		else:
			print ('.:: Conta Derrubada/Bloqueada ::.') 
			quit()

def enviarMongo(vetorImg,linkPerfil,fotoThumb):
	print ('.:: Alterando Registro Da Oferta ::.')
	print ('00 / {}\n'.format(fotoThumb))
	mydb.ofertasFacebook.update_one({
		'linkPerfil':linkPerfil
		},{
			"$set": {
			'linkFotosHosp' : vetorImg,
			'fotosColetadas' : True,
			'fotosThumb' : fotoThumb,
		}},
			upsert=False)

def enviarMongo0(linkPerfil,fotoThumb):
	print ('.:: Alterando Registro Da Oferta ::.')
	print ('01 / {}\n'.format(str(fotoThumb)))
	mydb.ofertasFacebook.update_one({
		'linkPerfil':linkPerfil
		},{
			"$set": {
			'fotosColetadas' : True,
			'fotosThumb' : fotoThumb,
		}},
			upsert=False)

databaseInfos = conexao['Infos'] #Buscamos o Email e Senha das contas no MongoDB
buscaLogin = databaseInfos.accountsFacebook.find_one({'status':'disponivel'})
emailLogin = buscaLogin['emailConta']
senhaLogin = buscaLogin['senhaConta']

criaSession(emailLogin,senhaLogin)