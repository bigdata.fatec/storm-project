from bs4 import BeautifulSoup
import requests 
import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from datetime import timezone
from datetime import timedelta
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from http.cookiejar import LWPCookieJar

def getData(string):

	diferencaZonas = timedelta(hours=-3)
	fusoHorario = timezone(diferencaZonas)

	now = datetime.datetime.now().astimezone(fusoHorario)
	
	vetorMeses = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro']
	vetorMesesNum = ['01','02','03','04','05','06','07','08','09','10','11','12']

	dicionario = dict(zip(vetorMeses,vetorMesesNum))

	analise = process.extract(string, vetorMeses,limit=1)
	analiseFrase = str(analise).split(',')
	perguntaAnalise = str(analiseFrase[0]).replace("'","").replace('(','').replace('[','')
	porcentagemAnalise = str(analiseFrase[1]).replace("'","").replace(')','').replace(']','')
	porcentagemAnalise = str(porcentagemAnalise).strip()

	extrairDia = re.search(r'(\d{2} de)',string)

	extrairAno = re.search(r'\b\d{4}\b',string)

	extrairHora = re.search(r'(\d{2}:\d{2})',string)

	extrairHoraSingular = re.search(r'(\d{1} h)',string)

	extrairHoraSingular2 = re.search(r'(\d{2} h)',string)

	extrairMinutosSingular = re.search(r'(\d{2} min)',string)

	if extrairMinutosSingular:
		horaOficial = extrairMinutosSingular.group()
		horaOficial = str(horaOficial).replace(' min','')
		horaOficial = now - datetime.timedelta(minutes=int(horaOficial))
		horaOficial = horaOficial.timestamp()
		return horaOficial

	if extrairHoraSingular:
		horaOficial = extrairHoraSingular.group()
		horaOficial = str(horaOficial).replace(' h','')
		horaOficial = now - datetime.timedelta(hours=int(horaOficial))
		horaOficial = horaOficial.timestamp()
		return horaOficial

	if extrairHoraSingular2:
		horaOficial = extrairHoraSingular2.group()
		horaOficial = str(horaOficial).replace(' h','')
		horaOficial = now - datetime.timedelta(hours=int(horaOficial))
		horaOficial = horaOficial.timestamp()
		return horaOficial

	if str(string).find('ontem'):
		pass

	if str(string) == 'agora mesmo':
		horaOficial = now
		horaOficial = horaOficial.timestamp()
		return horaOficial

	if extrairDia:
		diaOficial = extrairDia.group()
		diaOficial = str(diaOficial).replace(' de','')
		#print ('DIA : '+str(diaOficial))
	else:
		diaOficial = now.day
		#print ('DIA : '+str(diaOficial))

	if int(porcentagemAnalise) > 85:
		mesInt = dicionario[perguntaAnalise]
	else:
		mesInt = ''
	if str(mesInt) == '':
		mesInt = now.month
		if (len(str(mesInt))) == 1:
			mesInt = '0'+str(mesInt)
	#print ('MES : '+str(mesInt))

	if extrairAno:
		anoOficial = extrairAno.group()
		#print ('ANO : '+str(anoOficial))
	else:
		anoOficial = now.year
		#print ('ANO : '+str(anoOficial))

	if extrairHora:
		horaOficial = extrairHora.group()
		#print ('HORA : '+str(horaOficial))

	date = datetime.datetime.strptime(str(diaOficial)+' '+str(mesInt)+' '+str(anoOficial)+' '+str(horaOficial)+':00', '%d %m %Y %H:%M:%S')
	date = date.timestamp()
	return date
'''
12 de fevereiro de 2017 as 12:22 (feito)
12 de abril as 12:22 (feito)
ontem as 12:22 (feito)
48 min (feito)
12 h (feito)
agora mesmo (feito)
'''

#string = 'agora mesmo'
string = 'ontem as 12:12'

print (getData(string))