from bs4 import BeautifulSoup
import requests 
import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from datetime import timedelta
from datetime import timezone
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from http.cookiejar import LWPCookieJar

conexao = pymongo.MongoClient('mongodb+srv://botFacebook:bc123456!@storm-project-eu6jj.mongodb.net/test')
mydb = conexao['StormProject']

global s

s = requests.Session()

def criaSession(login, senha): #Função para armazenamento de Cookies Locais
		s.cookies = LWPCookieJar('cookiejar')
		if not os.path.exists('cookiejar'):
			print('.:: Armazenando Cookies ::.')
			s.cookies.save()
			loginFacebook(login, senha)
		else:
			print('.:: Carregando Cookies ::.')
			s.cookies.load(ignore_discard=True)
			req = s.get('https://m.facebook.com/')
			soup = BeautifulSoup(req.content,'html.parser')
			
			textoSoup = str(soup)
			if textoSoup.find("Participe do Facebook") != 0:
				loginFacebook(login, senha)
		s.cookies.save(ignore_discard=True)

def loginFacebook(email,senha): #Função para Login no Facebook
	vetor1 = []
	vetor2 = []

	req = requests.get('https://m.facebook.com/')
	soup = BeautifulSoup(req.content,'html.parser')

	for x in soup.find_all('input'):
		requisicao = x['name']
		if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
			vetor1.append(requisicao)
			resposta = x['value']
			vetor2.append(resposta)
			
	dicionario = dict(zip(vetor1,vetor2))

	dicionario['email'] = email
	dicionario['pass'] = senha
	dicionario['login'] = 'Log In'

	r = s.post('https://m.facebook.com/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&amp;refid=8', data=dicionario)
	timeRandom = random.randint(8, 20)
	time.sleep(timeRandom)
	buscaMongo = mydb.gruposFacebook.find({"coletado":False})
	for resultados in buscaMongo:
		linkGrupo = resultados['linkGrupos']
		nomeGrupo = resultados['nomeGrupos']
		idGrupo = resultados['_id']
		print ('.:: Coletando Ofertas Do Grupo : '+str(nomeGrupo)+' ::.')
		firstReq = s.get(linkGrupo)
		firstLevel(firstReq,linkGrupo,nomeGrupo,idGrupo)
		time.sleep(timeRandom)

def firstLevel(requisicao,linkGrupo,nomeGrupo,idGrupo): #Nesta função, iremos encontrar os links de cada oferta individualmente.
	soup = BeautifulSoup(requisicao.content,'html.parser')

	nome = soup.select('div#m_group_stories_container div div div div div h3 span strong a')
	linkAnuncioBruto = soup.select('div#m_group_stories_container')
	for dados in linkAnuncioBruto:
		soupPrimeiro = BeautifulSoup(str(dados),'html.parser')
		linkPost = soupPrimeiro.find_all('div',{'data-ft':'{"tn":"*W"}'})
		for x in linkPost:
			soupSegundo2 = BeautifulSoup(str(x),'html.parser')
			textos = soup.find_all('a')
			for xx in textos:
				if xx.text == 'História completa':
					linkPosts = 'https://m.facebook.com'+str(xx['href'])
					print ('.:: Oferta Encontrada! Redirecionando... ::.')
					requisicaoPosts = s.get(linkPosts)
					secondLevel(requisicaoPosts,linkPosts,linkGrupo,nomeGrupo,idGrupo)

def secondLevel(requisicao,linkPosts,linkGrupo01,nomeGrupo01,idGrupo): #Requisitamos cada oferta individualmente e analisamos uma por uma.
	arrayFotos = []

	soupTerceiro = BeautifulSoup(requisicao.content,'html.parser')
	textAnuncio = soupTerceiro.find_all('div',{'data-ft':'{"tn":"H"}'})
	for textosBruto in textAnuncio: # Buscamos os textos de cada oferta
		texto = textosBruto.text
		texto = str(texto).strip().replace('\n','').replace('\r','')
	print ('.:: Texto Anuncio Coletado ::.')
	nomeAndLink = soupTerceiro.select('div#objects_container div div div div div div table tbody tr td div h3 span strong a')
	if len(nomeAndLink) == 0:
		nomeAndLink = soupTerceiro.select('div#objects_container div div div div div div table tbody tr td div h3 a')
	contadorLinhas = 1
	for nomeLink in nomeAndLink: # Buscamos o nome e o link do Perfil e do Grupo
		nome = nomeLink.text
		link = nomeLink['href']
		link = str(link).split('fref')
		link = str(link[0][:-1])
		linkCompleto = 'https://m.facebook.com'+str(link)
		print (linkCompleto)
		if (contadorLinhas % 2 != 0): 
			nomePerfil = nome
			linkPerfil = linkCompleto
			linkDirect = 'https://www.messenger.com/t'+str(link)
			print ('.:: Nome e Link Do Perfil Coletado ::.')
		else:
			nomeGrupo = nome
			linkGrupo = linkCompleto
			print ('.:: Nome e Link Do Grupo Coletado ::.')
		contadorLinhas+=1

	fotosPost = soupTerceiro.find_all('div',{'data-ft':'{"tn":"E"}'})
	for fotos in fotosPost: # Buscamos todas as fotos de cada oferta e coletamos seus links
		soupQuarto = BeautifulSoup(str(fotos),'html.parser')
		buscaFotos = soupQuarto.find_all('a')
		for linkFotos in buscaFotos:
			linkFotosCompletos = 'https://m.facebook.com'+str(linkFotos['href'])
			arrayFotos.append(linkFotosCompletos)

	print ('.:: Fotos Da Oferta Coletadas ::.')
	
	horaPost = soupTerceiro.find_all('div',{'data-ft':'{"tn":"*W"}'})
	for hora in horaPost: # Buscamos a hora que foi efetuado cada Post.
		soupQuinto = BeautifulSoup(str(hora),'html.parser')
		horaPostCompleta = soupQuinto.find('abbr')
		horaPostCompleto = horaPostCompleta.text
		horaPostOficial = getData(horaPostCompleto)
		horaOficial = datetime.datetime.isoformat(horaPostOficial)
		try:
			d = datetime.datetime.strptime(horaOficial, "%Y-%m-%dT%H:%M:%S.%f")
		except:
			d = datetime.datetime.strptime(horaOficial, "%Y-%m-%dT%H:%M:%S")
		diferencaZonas = timedelta(hours=-3)
		fusoHorario = timezone(diferencaZonas)
		hora = d.astimezone(fusoHorario)
	 # Chamamos a função para tratamento da hora dos Posts
	print ('.:: Hora de Post Coletada - '+str(hora)+' ::.')
	try:
		linkPerfil = str(linkPerfil).replace('m.facebook','www.facebook')
		linkGrupo = str(linkGrupo).replace('m.facebook','www.facebook')
		linkPosts = str(linkPosts).replace('m.facebook','www.facebook')
	except:
		linkGrupo = str(linkGrupo01)
		nomeGrupo = str(nomeGrupo01)
	enviarMongo(nomePerfil,linkPerfil,nomeGrupo,linkGrupo,texto,hora,arrayFotos,linkPosts,linkDirect,idGrupo)
	updateGrupos(str(nomeGrupo))
	arrayFotos.clear()
	timeRandom = random.randint(8, 20)
	time.sleep(timeRandom)

def updateGrupos(nomeGrupo):
	buscaMongo = mydb.gruposFacebook.find_one({'nomeGrupos':nomeGrupo})
	quantidade = buscaMongo['quantidadeOfertas']
	quantidadeOfertas = int(quantidade)+1
	print (quantidadeOfertas)
	mydb.gruposFacebook.update({
		'nomeGrupos':nomeGrupo,
	},{'$set':{
		'quantidadeOfertas' : quantidadeOfertas,
		'coletado' : True
	}})

def enviarMongo(nomePerfil,linkPerfil,nomeGrupo,linkGrupo,texto,horaPostCompleto,arrayFotos,linkPosts,linkDirect,idGrupo):
	texto = str(texto).replace('(Vendido)','')
	buscaMongo = mydb.ofertasFacebook.find_one({'linkPost':linkPosts}) #Verifificamos se o registro já existe
	if buscaMongo == None:
		print ('.:: Novo Registro Inserido No BD ::.')
		mydb.ofertasFacebook.insert({
			'nomePerfil': nomePerfil,
			'linkPerfil': linkPerfil,
			'nomeGrupo' : nomeGrupo,
			'linkGrupo' : linkGrupo,
			'idGrupo' : idGrupo,
			'textoOferta' : texto,
			'horaOferta' : horaPostCompleto,
			'linkOferta' : linkPosts,
			'fotosOferta' : arrayFotos,
			'fotosColetadas' : False,
			'directLink' : linkDirect,
		})
	else:
		print ('.:: Registro Já Existente No BD ::.')

	print ('############################\n')
	
	#print ('Nome Perfil : '+str(nomePerfil))
	#print ('Link Perfil : '+str(linkPerfil))
	#print ('Nome Grupo : '+str(nomeGrupo))
	#print ('Link Grupo : '+str(linkGrupo))
	#print ('Oferta : '+str(texto))
	#print ('Hora Post : '+str(horaPostCompleto))
	#print ('Array Fotos : '+str(arrayFotos))
	#print ('Link Post : '+str(linkPosts))
	#print ('########################################')

def getData(string):

	now = datetime.datetime.now()
	
	vetorMeses = ['janeiro','fevereiro','março','abril','maio','junho','julho','agosto','setembro','outubro','novembro','dezembro']
	vetorMesesNum = ['01','02','03','04','05','06','07','08','09','10','11','12']

	dicionario = dict(zip(vetorMeses,vetorMesesNum))

	analise = process.extract(string, vetorMeses,limit=1)
	analiseFrase = str(analise).split(',')
	perguntaAnalise = str(analiseFrase[0]).replace("'","").replace('(','').replace('[','')
	porcentagemAnalise = str(analiseFrase[1]).replace("'","").replace(')','').replace(']','')
	porcentagemAnalise = str(porcentagemAnalise).strip()

	extrairDia = re.search(r'(\d{2} de)',string)

	extrairAno = re.search(r'\b\d{4}\b',string)

	extrairHora = re.search(r'(\d{2}:\d{2})',string)

	extrairHoraSingular = re.search(r'(\d{1} h)',string)

	extrairHoraSingular2 = re.search(r'(\d{2} h)',string)

	extrairMinutosSingular = re.search(r'(\d{2} min)',string)

	extrairOntem = re.search(r'(ontem)',string)

	if extrairMinutosSingular:
		horaOficial = extrairMinutosSingular.group()
		horaOficial = str(horaOficial).replace(' min','')
		horaOficial = now - datetime.timedelta(minutes=int(horaOficial))
		return horaOficial

	if extrairHoraSingular:
		horaOficial = extrairHoraSingular.group()
		horaOficial = str(horaOficial).replace(' h','')
		horaOficial = now - datetime.timedelta(hours=int(horaOficial))
		return horaOficial

	if extrairHoraSingular2:
		horaOficial = extrairHoraSingular2.group()
		horaOficial = str(horaOficial).replace(' h','')
		horaOficial = now - datetime.timedelta(hours=int(horaOficial))
		return horaOficial

	if extrairOntem:
		horaOficial = now
		return horaOficial

	if str(string) == 'agora mesmo':
		horaOficial = now
		return horaOficial

	if extrairDia:
		diaOficial = extrairDia.group()
		diaOficial = str(diaOficial).replace(' de','')
		#print ('DIA : '+str(diaOficial))
	else:
		diaOficial = now.day
		#print ('DIA : '+str(diaOficial))

	if int(porcentagemAnalise) > 85:
		mesInt = dicionario[perguntaAnalise]
	else:
		mesInt = ''
	if str(mesInt) == '':
		mesInt = now.month
		if (len(str(mesInt))) == 1:
			mesInt = '0'+str(mesInt)
	#print ('MES : '+str(mesInt))

	if extrairAno:
		anoOficial = extrairAno.group()
		#print ('ANO : '+str(anoOficial))
	else:
		anoOficial = now.year
		#print ('ANO : '+str(anoOficial))

	if extrairHora:
		horaOficial = extrairHora.group()
		#print ('HORA : '+str(horaOficial))
	try:
		date = datetime.datetime.strptime(str(diaOficial)+' '+str(mesInt)+' '+str(anoOficial)+' '+str(horaOficial)+':00', '%d %m %Y %H:%M:%S')
		return date
	except:
		horaOficial = '12:00'
		date = datetime.datetime.strptime(str(diaOficial)+' '+str(mesInt)+' '+str(anoOficial)+' '+str(horaOficial)+':00', '%d %m %Y %H:%M:%S')
		return date

databaseInfos = conexao['Infos'] #Buscamos o Email e Senha das contas no MongoDB
buscaLogin = databaseInfos.accountsFacebook.find_one({'status':'disponivel'})
emailLogin = buscaLogin['emailConta']
senhaLogin = buscaLogin['senhaConta']

criaSession(emailLogin,senhaLogin)