from bs4 import BeautifulSoup
import requests 
import pymongo
from lxml import html
import time
import random
import re
import os
import logging
import datetime
from http.cookiejar import LWPCookieJar

conexao = pymongo.MongoClient('mongodb+srv://botFacebook:bc123456!@storm-project-eu6jj.mongodb.net/test')
mydb = conexao['StormProject']

global s

s = requests.Session()

def criaSession(login, senha): #Função para armazenamento de Cookies Locais
		s.cookies = LWPCookieJar('cookiejar')
		if not os.path.exists('cookiejar'):
			print('.:: Armazenando Cookies ::.')
			s.cookies.save()
			loginFacebook(login, senha)
		else:
			print('.:: Carregando Cookies ::.')
			s.cookies.load(ignore_discard=True)
			req = s.get('https://m.facebook.com/')
			soup = BeautifulSoup(req.content,'html.parser')
			
			textoSoup = str(soup)
			if textoSoup.find("Participe do Facebook") != 0:
				loginFacebook(login, senha)
		s.cookies.save(ignore_discard=True)

def loginFacebook(email,senha): #Função para Login no Facebook
	vetor1 = []
	vetor2 = []

	req = requests.get('https://m.facebook.com/')
	soup = BeautifulSoup(req.content,'html.parser')

	for x in soup.find_all('input'):
		requisicao = x['name']
		if str(requisicao) == 'lsd' or str(requisicao) == 'li' or str(requisicao) == 'm_ts':
			vetor1.append(requisicao)
			resposta = x['value']
			vetor2.append(resposta)
			
	dicionario = dict(zip(vetor1,vetor2))

	dicionario['email'] = email
	dicionario['pass'] = senha
	dicionario['login'] = 'Log In'

	r = s.post('https://m.facebook.com/login.php?refsrc=https%3A%2F%2Fm.facebook.com%2F&amp;refid=8', data=dicionario)
	timeRandom = random.randint(8, 20)
	time.sleep(timeRandom)
	
def getLinksGroups():
	
	listaNomes = []
	listaLinks = []

	busca = mydb.parametrosPesquisaGrupos.find() #Aqui buscamos os parametros de pesquisa no MongoDB
	for contador in busca:
		timeRandom = random.randint(8, 20)
		time.sleep(timeRandom)
		
		nomeOlx = contador['gruposPesquisa']
		nomeCorreto = str(nomeOlx).replace(' ','+') #Substituimos os espaços por '+' para completar a URL

		site = requests.get('https://m.facebook.com/graphsearch/str/'+str(nomeCorreto)+'/keywords_groups?tsid=0.5104155088381142&source=pivot')
		
		soup = BeautifulSoup(site.content,'html.parser')
		mapeamento = soup.select('div#objects_container div div div div div div table tbody tr td a')
		
		for a in mapeamento: #Coletamos os nomes e links dos grupos presentes no resultado da pesquisa
			nomeGrupo = a.text
			if str(nomeGrupo) != '':
				listaNomes.append(nomeGrupo)
			links = a['href']
			linkCompleto = 'https://m.facebook.com'+str(links)
			listaLinks.append(linkCompleto)
			listaSemRepetidos = remove_repetidos(listaLinks)
			
		for nome,link in zip(listaNomes,listaLinks):
			enviarMongo(nome,link) #Enviamos os registros para o MongoDB
		listaNomes.clear()
		listaNomes.clear()

def remove_repetidos(lista): #Filtramos para não haver nomes e links repetidos.
	l = []
	for i in lista:
		if i not in l:
			l.append(i)
	l.sort()
	return l

def enviarMongo(nomeGrupo,linkGrupo):
	buscaMongo = mydb.gruposFacebook.find_one({'nomeGrupos':str(nomeGrupo)})  #Verifificamos se o registro já existe
	if buscaMongo == None:
		print ('.:: Novo Registro ::.')
		mydb.gruposFacebook.insert({
			"nomeGrupos" : str(nomeGrupo),
			"linkGrupos" : str(linkGrupo),
		})
	else:
		print ('.:: Registro Já Existente ::.')

databaseInfos = conexao['Infos'] #Buscamos o Email e Senha das contas no MongoDB
buscaLogin = databaseInfos.accountsFacebook.find_one({'status':'disponivel'})
emailLogin = buscaLogin['emailConta']
senhaLogin = buscaLogin['senhaConta']

criaSession(emailLogin,senhaLogin)
getLinksGroups()